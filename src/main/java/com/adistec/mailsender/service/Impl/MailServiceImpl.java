package com.adistec.mailsender.service.Impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.CharEncoding;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.adistec.mailsender.entity.Mail;
import com.adistec.mailsender.repository.MailRepository;
import com.adistec.mailsender.service.MailService;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Service
public class MailServiceImpl implements MailService {
	
	private static final Logger LOG = LoggerFactory.getLogger(MailServiceImpl.class);
	
	@Autowired
    private TaskExecutor taskExecutor;
	
    @Value("${spring.mail.account.devsend}")
    private String emailFrom;
    
    @Value("${spring.mail.account.webdev}")
    private String webDevMail;
    
    @Value("#{'${spring.mail.account.feedback}'.split(',')}") 
    private String[] feedbackTo;
    
    @Value("${spring.mail.template.default}")
    private String defaultTemplate;
    
    @Value("${spring.mail.properties.feedbackSubject}")
    private String feedbackSubject;
	
	@Value("${spring.mail.account.devsend}")
    private String devsendAccount;
	
	@Value("${spring.mail.account.noreply}")
    private String noreply;
	
	@Value("${spring.mail.template.recovery}")
    private String recoveryTemplate;
	
	@Value("${spring.mail.template.feedback}")
    private String feedbackTemplate;
	
	@Value("${spring.profiles.active}")
	private String activeProfile;
	
    private JavaMailSender emailSender;
    
    @Autowired
    MailRepository mailRepository;
    
    @Autowired
    private SpringTemplateEngine templateEngine;
    
    private void sendEmail(String[] to, String from, String subject, boolean isMultipart, boolean isHtml, HashMap<String,Resource> inlines,
		String content, String[] copyTo, String[] cco, String application) throws MessagingException, IOException {
    	if (to == null) {
    		to = new String[] {webDevMail};
    	}
    	if (from == null)
    		from = emailFrom;
    	Mail mail = getMailFromProperties(to, from, subject, isMultipart, isHtml, inlines, content, copyTo, cco, application);
    	send(mail);
    }

	private Mail getMailFromProperties(String[] to, String from, String subject, boolean isMultipart, boolean isHtml,
			HashMap<String, Resource> inlines,String content, String[] copyTo, String[] cco, String application)
			throws IOException {
		String toString = String.join(",", to);
    	String ccString = copyTo != null ? String.join(",",copyTo) : null;
    	String bccString = cco != null ? String.join(",",cco) : null;
    	HashMap<String, String> inlinesStringMap = getStringMapFromResources(inlines);
    	String inlinesString = new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false).writeValueAsString(inlinesStringMap);
    	Mail mail = new Mail(toString, from, subject, new Date(), null, isHtml, isMultipart, ccString, bccString, inlinesString, content, application);
		setMailServer(mail);
		return mail;
	}

	private void setMailServer(Mail mail) {
		if (((JavaMailSenderImpl) emailSender).getUsername() != null) {
			mail.setMailServer("office");
		} else {
			mail.setMailServer("open");
		}
	}

	@Override
	public void sendEmailWithContent(String[] sentToEmail, String subject, String content, String from,
						   String inlines, String[] copyTo, String[] cco, String application) throws MessagingException, IOException {
		HashMap<String, String> inlinesStringMap = parseStringToHashMap(inlines);
		HashMap<String, Resource> inlinesResourceMap = null;
		if (inlinesStringMap != null) {
			inlinesResourceMap = parseInlinesMap(inlinesStringMap);
		}
		sendEmail(sentToEmail, from, subject, inlines != null, true, inlinesResourceMap, content, copyTo, cco, application);
	}

	@Override
	public List<Mail> getMailsSent(String to, String application){
    	return mailRepository.findByToAndApplicationAndStatusOrderBySendDateDesc(to, application, "delivered");
	}

	private HashMap<String, String> getStringMapFromResources(HashMap<String, Resource> inlines) throws IOException {
		HashMap<String, String> inlinesStringMap = new HashMap<>();
    	for (Map.Entry<String, Resource> entry : inlines.entrySet()) {
        	byte[] readFileToByteArray = new byte[] {};
			readFileToByteArray = IOUtils.toByteArray(entry.getValue().getInputStream());
			
        	String encodedImage = Base64.getEncoder().encodeToString(readFileToByteArray);
        	inlinesStringMap.put(entry.getKey(), encodedImage);
        }
		return inlinesStringMap;
	}
    
    public void retryFailedMails(JavaMailSender officeServer, JavaMailSender openServer) {
    	List<Mail> failedMails = mailRepository.findByStatus("failed");
    	for(Mail mail: failedMails) {
    		try {
    			if (mail.getMailServer().equalsIgnoreCase("office"))
    				emailSender = officeServer;
    			else
    				emailSender = openServer;
				send(mail);
			} catch (IOException | MessagingException e) {
				LOG.error("Resend mail failed: ID = " + mail.getId() + ". Attempts: " + mail.getAttempts());
			}
    	}
    }
    
    private void send(Mail mail) throws IOException, MessagingException {
    	HashMap<String,Resource> inlines = null;
    	if (mail.getInlines() != null)
    		inlines = parseInlinesMap(parseStringToHashMap(mail.getInlines()));
		String[] to = mail.getTo() != null ? mail.getTo().split(",") : null;
		String[] cc = mail.getCc() != null ? mail.getCc().split(",") : null;
		String[] bcc = mail.getBcc() != null ? mail.getBcc().split(",") : null;
		MimeMessage mimeMessage = setMimeMessage(to, mail.getFrom(), mail.getSubject(), mail.isMultipart(), mail.isHtml(), inlines, mail.getContent(), cc, bcc);
		mail.setStatus("pending");
    	Mail localMail = mailRepository.saveAndFlush(mail);
        taskExecutor.execute(() -> {
			try {
				emailSender.send(mimeMessage);
				localMail.setSendDate(new Date());
				localMail.setStatus("delivered");
				LOG.info("Mail to: " + localMail.getTo() + " with Subject: " + localMail.getSubject() + " sent. Mail ID = " + localMail.getId());
			} catch (Exception e) {
				localMail.setStatus("failed");
				LOG.info("MAIL EXCEPTION: " + e.getMessage());
			}
			localMail.setAttempts(localMail.getAttempts() + 1);
			mailRepository.saveAndFlush(localMail);
		});
	}
    
    @Override
	public void sendEmailWithTemplate(String sentToEmail, String subject, String template, String from,
			String templateVariables, String inlines, String copyTo, String cco, String application) throws IOException, MessagingException {
    	String[] sentToEmailArray = parseStringToStringArray(sentToEmail);
    	String[] copyToArray = parseStringToStringArray(copyTo);
    	String[] ccoArray = parseStringToStringArray(cco);
		HashMap<String, String> inlinesStringMap = parseStringToHashMap(inlines);
		HashMap<String, Resource> inlinesResourceMap = null;
		if (inlinesStringMap != null) {
			inlinesResourceMap = parseInlinesMap(inlinesStringMap);
		}
		String content = setContent(inlinesResourceMap, templateVariables, template);
        sendEmail(sentToEmailArray, from, subject, inlines != null, true, inlinesResourceMap, content,
			copyToArray, ccoArray, application);
	}
    
	private String[] parseStringToStringArray(String source) throws IOException {
    	return source != null ? (String[]) new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).readValue(source, new TypeReference<String[]>() {}) : null;
	}

	@Override
	public void sendFeedback(String templateVariables, String appName, String subject, String application) throws MessagingException, IOException {
    	String logoPath = "static/images/" + appName + ".png";
    	HashMap<String, Resource> inlines = null;
		inlines = setInlineFromPath(inlines, logoPath, "logo");
		String content = setContent(inlines, templateVariables, feedbackTemplate);
		sendEmail(feedbackTo, devsendAccount, subject, inlines != null, true,inlines,content, null, null, application);
	}
    
	@Override
	public void sendRecovery(String templateVariables, String subject, String username, String application) throws MessagingException, IOException {
		String blueLogo = "static/images/adistec-logo-blue.png";
		HashMap<String, Resource> inlines = null;
		inlines = setInlineFromPath(inlines, blueLogo, "blueLogo");
		String content = setContent(inlines, templateVariables, recoveryTemplate);
		sendEmail(new String[] {username}, noreply, subject, inlines != null, true,inlines, content,
			null, null, application);
	}

	private HashMap<String, Resource> setInlineFromPath(HashMap<String, Resource> inlines, String path, String key) {
		if (this.getClass().getClassLoader().getResource(path) != null) {
			inlines = new HashMap<>();
			inlines.put(key, new ClassPathResource(path));
		}
		return inlines;
	}

    public List<String> getTemplateFiles() throws IOException  {
    	List<String> templates = new ArrayList<>();
    	ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    	for (Resource resource: resolver.getResources("classpath*:/templates/*.html")){
    	    templates.add(resource.getFilename());
    	}
    	return templates;
    }

	@SuppressWarnings("unchecked")
	private HashMap<String, String> parseStringToHashMap(String source) throws IOException {
		return source != null ? (HashMap<String, String>) new ObjectMapper().configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true).readValue(source, HashMap.class) : null;
	}
	
	private HashMap<String, Resource> parseInlinesMap(HashMap<String, String> inlinesStringMap) {
		HashMap<String, Resource> inlinesResourceMap = new HashMap<>();
		for (Map.Entry<String, String> entry : inlinesStringMap.entrySet()) {
			inlinesResourceMap.put(entry.getKey(), getResourceFromBase64String(entry.getValue()));
		}
		return inlinesResourceMap;
	}
	
	private Resource getResourceFromBase64String(String input) {
		ByteArrayResource resource;
		byte[] byteA = Base64.getDecoder().decode(input);
		resource = new ByteArrayResource(byteA);
		return resource;
	}
	
	private void setContext(Context ctx, HashMap<String, String> variables, HashMap<String, Resource> inlines) {
    	if (variables != null) {
    		variables.entrySet().stream().forEach(variable -> ctx.setVariable(variable.getKey(),variable.getValue()));
        }
        if(inlines != null) {
            inlines.entrySet().stream().forEach(inline ->
            ctx.setVariable(inline.getKey(), inline.getKey())
            );
        }
    }
	
	private MimeMessage setMimeMessage(String[] to, String from, String subject, boolean isMultipart, boolean isHtml,
			HashMap<String, Resource> inlines, String content, String[] copyTo, String[] cco) throws MessagingException {
		MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
        message.setTo(to);
        message.setFrom(from);
        message.setSubject(subject);
        message.setText(content, isHtml);
        if (copyTo != null) 
        	message.setCc(copyTo);
        if (cco != null) 
        	message.setBcc(cco);
        if(inlines != null) {
            for (Map.Entry<String, Resource> entry : inlines.entrySet()) {
				message.addInline(entry.getKey(), entry.getValue(), "image/png");
            }
        }
		return mimeMessage;
	}

	private String setContent(HashMap<String, Resource> inlines, String templateVariables, String template)
			throws IOException {
		HashMap<String, String> variablesMap = parseStringToHashMap(templateVariables);
    	Context context = new Context();
    	setContext(context, variablesMap, inlines);

    	return templateEngine.process(template, context);
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.emailSender = mailSender;
	}

}