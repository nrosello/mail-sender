package com.adistec.mailsender.dto;

import com.adistec.mailsender.entity.Mail;

import java.util.Date;

public class MailNotificationsDTO {
    private String sentToEmail;
    private String subject;
    private Date sentDate;
    private String content;

    public MailNotificationsDTO(){}

    public MailNotificationsDTO(Mail mail){
        this.sentToEmail = mail.getTo();
        this.subject = mail.getSubject();
        this.sentDate = mail.getSendDate();
        this.content = mail.getContent();
    }

    public String getSentToEmail() {
        return sentToEmail;
    }

    public void setSentToEmail(String sentToEmail) {
        this.sentToEmail = sentToEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
